# update_email_in_csv



## Bash Task 1

- Update the name column to have the first letter name/surname, rest lowercase. 
- Change the email column to use the format: first letter of the name + full surname (all lowercase) @abc domain. If emails duplicate, add location_id.
The script should take the path to accounts.csv as an argument. 
Bash script automatically creates accounts_new.csv with updated columns.

## Example:
```bash
./task1.sh accounts.csv
```
### was:
```csv
8,6,Bart charlow,Executive Director,,
9,7,Bart Charlow,Executive Director,,
```
### became:
```csv
8,6,Bart Charlow,Executive Director,bcharlow6@abc.com,
9,7,Bart Charlow,Executive Director,bcharlow7@abc.com,
```

## Bash Task 2

Converts output.txt into output.json

```bash
[ Asserts Samples ], 1..2 tests
-----------------------------------------------------------------------------------
not ok  1  expecting command finishes successfully (bash way), 7ms
ok  2  expecting command prints some message (the same as above, bats way), 10ms
-----------------------------------------------------------------------------------
1 (of 2) tests passed, 1 tests failed, rated as 50%, spent 17ms
```

### should be output.json:
```json
{
 "testName": "Asserts Samples",
 "tests": [
   {
     "name": "expecting command finishes successfully (bash way)",
     "status": false,
     "duration": "7ms"
   },
   {
     "name": "expecting command prints some message (the same as above, bats way)",
     "status": true,
     "duration": "10ms"
   }
 ],
 "summary": {
   "success": 1,
   "failed": 1,
   "rating": 50,
   "duration": "17ms"
 }
}